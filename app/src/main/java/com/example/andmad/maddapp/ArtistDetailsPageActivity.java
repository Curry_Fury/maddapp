package com.example.andmad.maddapp;

import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.andmad.maddapp.database.DatabaseHandler;

public class ArtistDetailsPageActivity extends AppCompatActivity {

    protected DatabaseHandler db;
    public static final int DATABASE_VERSION = 1;
    public static String DATABASE_NAME = "Music.db";
    TextView musicLyrics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details_page);

        db = new DatabaseHandler(this, DATABASE_NAME, null, DATABASE_VERSION);

        musicLyrics = (TextView) findViewById(R.id.LyricsTxt);

        musicLyrics.setText(SongListActivity.showLyrics);


    }
}
