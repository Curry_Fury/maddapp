package com.example.andmad.maddapp;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.andmad.maddapp.database.DatabaseHandler;

import java.util.ArrayList;


public class ArtistListActivity extends AppCompatActivity {

    ImageButton btnAddArtist;

    DatabaseHandler db;

    public static final  int DATABASE_VERSION =1;
    public static final String DATABASE_NAME = "Music.db";

    private ListView ArtistList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_list);

        ArtistList = (ListView) findViewById(R.id.Artist_List);
        db = new DatabaseHandler(this, DATABASE_NAME,null,DATABASE_VERSION);
        ListContent();


        btnAddArtist = (ImageButton) findViewById(R.id.add_btn);

       btnAddArtist.setOnClickListener(new View.OnClickListener(){
        public void onClick (View v){
           startActivity(new Intent(getApplicationContext(),ArtistAddActivity.class));
        }

       });
    }

    private void ListContent() {
        ArrayList<String> theList = new ArrayList<>();
        Cursor data = db.getListContent();

        while (data.moveToNext()){
            theList.add(data.getString(1));
        }
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, theList);
        ArtistList.setAdapter(adapter);
    }


}
