package com.example.andmad.maddapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomePageActivity extends AppCompatActivity {


    Button Artist_Button;
    Button Song_Button;
    Button Lyrics_Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        Artist_Button = (Button) findViewById(R.id.Artist_Btn);
        Song_Button = (Button) findViewById(R.id.Song_Btn);


    Artist_Button.setOnClickListener(new View.OnClickListener(){
        public void onClick (View v){
            startActivity(new Intent(getApplicationContext(),ArtistListActivity.class));
        }
    });

        Song_Button.setOnClickListener(new View.OnClickListener(){
            public void onClick (View v){
                startActivity(new Intent(getApplicationContext(),SongListActivity.class));
            }
        });



    }
}
