package com.example.andmad.maddapp;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.andmad.maddapp.database.DatabaseHandler;

import java.util.ArrayList;


public class SongListActivity extends AppCompatActivity {

    DatabaseHandler db;

    public static final  int DATABASE_VERSION =1;
    public static final String DATABASE_NAME = "Music.db";
    public static String showLyrics;

    private ListView MusicList;

    Button addSong;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_list);

        MusicList = (ListView) findViewById(R.id.Song_List);
        db = new DatabaseHandler(this, DATABASE_NAME,null,DATABASE_VERSION);
        listContent();

        MusicList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(view.getContext(),ArtistDetailsPageActivity.class);
                startActivity(myIntent);
                showLyricsOfSong(position);
            }


        });



        addSong = (Button) findViewById(R.id.AddSong_Btn);


        addSong.setOnClickListener(new View.OnClickListener(){
            public void onClick (View v){
                startActivity(new Intent(getApplicationContext(),ArtistAddActivity.class));
            }

        });
    }

    private void listContent() {
        ArrayList<String> theList = new ArrayList<>();
        Cursor data = db.getListContent();

        while (data.moveToNext()){
            theList.add(data.getString(2));
        }
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, theList);
        MusicList.setAdapter(adapter);
    }

    public  void showLyricsOfSong(int id){

        Cursor data = db.displayLyrics(id+1);

        while (data.moveToNext()){

           showLyrics = data.getString(3);

        }
    }
}
