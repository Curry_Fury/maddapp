package com.example.andmad.maddapp;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.andmad.maddapp.database.DatabaseHandler;

public class ArtistAddActivity extends AppCompatActivity {

    protected DatabaseHandler db;
    public  static final int DATABASE_VERSION =1;
    public static  final String DATABASE_NAME = "Music.db";

    Button SaveTodb;
    TextView ArtistNametodb;
    TextView SongNametodb;
    TextView Lyrictodatabase;
    String artistName;
    String songName;
    String Lyricstodb;
    AlertDialog.Builder build;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_add);

        SaveTodb = (Button) findViewById(R.id.Save_Btn);
        ArtistNametodb = (TextView) findViewById(R.id.Artist_Name);
        SongNametodb = (TextView) findViewById(R.id.Song_Name);
        Lyrictodatabase = (TextView) findViewById(R.id.Lyrics_Text);

        build = new AlertDialog.Builder(this);

        db = new DatabaseHandler(this, DATABASE_NAME,null,DATABASE_VERSION);

        SaveTodb.setOnClickListener(new View.OnClickListener(){
            public void onClick (View v){

                artistName= ArtistNametodb.getText().toString();
                songName= SongNametodb.getText().toString();
                Lyricstodb= Lyrictodatabase.getText().toString();

                db.addLyrics(artistName,songName,Lyricstodb);
                build.setTitle("Success");
                build.setMessage("Successfully Inserted");
                build.show();

            }
        });

    }


}
