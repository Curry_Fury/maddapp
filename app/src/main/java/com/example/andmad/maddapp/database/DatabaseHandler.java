package com.example.andmad.maddapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by andmad on 10/20/17.
 */


    public class DatabaseHandler extends SQLiteOpenHelper {

        public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {

            super(context, name, factory, version);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String SQL_CREATE_ENTRIES =
                    "CREATE TABLE " + Master.Music.TABLE_NAME + " (" +
                            Master.Music._ID + "INTEGER PRIMARY KEY," +
                            Master.Music.SONG_ARTIST + " TEXT," +
                            Master.Music.SONG_NAME + " TEXT," +
                            Master.Music.SONG_LYRICS + " TEXT)";
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            db.execSQL("DROP TABLE IF EXISTS" + Master.Music.TABLE_NAME);
            onCreate(db);
        }

        public  void addLyrics(String artist, String songName, String lyric){
            SQLiteDatabase db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(Master.Music.SONG_ARTIST, artist);
            values.put(Master.Music.SONG_NAME, songName);
            values.put(Master.Music.SONG_LYRICS, lyric);

            long newRowId = db.insert(Master.Music.TABLE_NAME, null, values);
        }


        public Cursor getListContent(){

            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT * FROM " + Master.Music.TABLE_NAME;
            Cursor data = db.rawQuery(query, null);
            return data;
        }

    public  Cursor displayLyrics(int givenID){
        SQLiteDatabase db =this.getWritableDatabase();

        String query = "SELECT * FROM " + Master.Music.TABLE_NAME + " WHERE "+Master.Music._ID +"="+givenID;
        Cursor c = db.rawQuery(query, null);
        return c;
    }


    }

