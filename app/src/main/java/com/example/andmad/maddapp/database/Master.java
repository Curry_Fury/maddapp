package com.example.andmad.maddapp.database;

import android.provider.BaseColumns;

/**
 * Created by andmad on 10/20/17.
 */


    public  final class Master {

        private Master(){

        }

        public  static  class Music implements BaseColumns {

            public static  final String TABLE_NAME = "Music";
            public static  final String SONG_ARTIST = "Artists";
            public static  final String SONG_NAME = "SongName";
            public static  final String SONG_LYRICS = "Lyrics";
        }
    }


